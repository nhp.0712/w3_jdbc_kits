public class Subject{
	private int id;
	private String name;

	public int getID(){return this.id;}
	public String getName(){return this.name;}

	public void setID(int id){ this.id = id;}
	public void setName(String name){ this.name = name;}

	
}