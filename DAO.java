public interface DAO<T>{
	public void add(T t);
	public void delete();
	public void search(T t);
	public void displayStuScore(T t);
	public void displaySubScore(T t);
}